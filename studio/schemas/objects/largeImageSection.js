export default {
  type: "object",
  name: "largeImageSection",
  title: "Large Image with text",
  fields: [
    {
      name: "heading",
      type: "string",
      title: "Heading",
    },
    {
      name: "text",
      type: "simplePortableText",
      title: "Text",
    },
    {
      name: "largeImage",
      type: "image",
      title: "Large Image",
    },
    {
      name: "smallImage",
      type: "image",
      title: "Small Image",
    },
  ],
  preview: {
    select: {
      heading: "heading",
      subtitle: "label",
      media: "largeImage",
    },
    prepare({ heading, media }) {
      return {
        title: `${heading}`,
        subtitle: "Large Image section",
        media,
      };
    },
  },
};
