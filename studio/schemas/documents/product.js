import { MdLocalDrink } from "react-icons/md";

export default {
  name: "product",
  title: "Product",
  type: "document",
  icon: MdLocalDrink,
  initialValue: {
    sold: false,
  },
  fields: [
    {
      name: "title",
      title: "Title",
      type: "string",
    },
    {
      name: "slug",
      title: "Slug",
      type: "slug",
      validation: (Rule) => Rule.required(),
      options: {
        source: "title",
        maxLength: 96,
      },
    },
    {
      name: "stockNumber",
      title: "Stock Number",
      type: "string",
    },
    {
      title: "Main image",
      name: "mainImage",
      type: "image",
      options: {
        hotspot: true,
      },
    },
    {
      title: "Secondary images",
      name: "secondaryImages",
      type: "array",
      of: [{ type: "image" }],
    },
    {
      name: "content",
      title: "Content",
      type: "simplePortableText",
    },
    {
      title: "Sold",
      name: "sold",
      type: "boolean",
    },
    {
      title: "Price",
      name: "price",
      type: "number",
    },
    {
      name: "categories",
      title: "Categories",
      type: "array",
      validation: (Rule) => Rule.required().max(1),
      of: [
        {
          type: "reference",
          to: { type: "productCategories" },
        },
      ],
    },
  ],

  preview: {
    select: {
      title: "title",
      media: "mainImage",
    },
  },
};
