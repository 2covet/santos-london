import { MdBook } from "react-icons/md";

export default {
  name: "book",
  title: "Book",
  type: "document",
  icon: MdBook,
  fields: [
    {
      name: "title",
      title: "Title",
      type: "string",
    },
    {
      name: "slug",
      title: "slug",
      type: "slug",
      validation: (Rule) => Rule.required(),
      options: {
        source: "title",
        maxLength: 96,
      },
    },
    {
      title: "Main Image",
      name: "mainImage",
      type: "image",
    },
    {
      title: "Secondary Images",
      name: "secondaryImages",
      type: "array",
      of: [{ type: "image" }],
    },
    {
      title: "PDF for download",
      name: "pdfDownload",
      type: "file",
      description: "Please only add PDF files.",
      storeOriginalFilename: true,
    },
    {
      name: "content",
      title: "Content",
      type: "simplePortableText",
    },
  ],

  preview: {
    select: {
      title: "title",
      media: "mainImage",
    },
  },
};
