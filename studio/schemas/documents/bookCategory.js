import { MdBook } from "react-icons/md";

export default {
  name: "bookCategory",
  type: "document",
  title: "Book Category",
  __experimental_actions: [/*"create",*/ "update", /*'delete',*/ "publish"],
  icon: MdBook,
  fields: [
    {
      name: "title",
      type: "string",
      title: "Book Title",
    },
    { name: "slug", title: "Slug", type: "slug" },
    {
      name: "chineseTitle",
      title: "Chinese Title",
      type: "string",
    },
    {
      title: "Book Image",
      name: "image",
      type: "image",
    },
  ],
};
