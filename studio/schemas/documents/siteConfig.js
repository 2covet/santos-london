export default {
  name: "siteConfig",
  type: "document",
  title: "Site configuration",
  __experimental_actions: [/*'create',*/ "update", /*'delete',*/ "publish"],
  fields: [
    {
      name: "title",
      type: "string",
      title: "Site title",
    },
    {
      title: "Brand logo",
      description:
        "Best choice is to use an SVG where the color are set with currentColor",
      name: "logo",
      type: "image",
      fields: [
        {
          name: "alt",
          type: "string",
          title: "Alternative text",
          description: "Important for SEO and accessiblity.",
          options: {
            isHighlighted: true,
          },
        },
      ],
    },
    {
      title: "Associates",
      name: "associates",
      type: "array",
      of: [
        {
          type: "image",
          fields: [
            { name: "title", type: "string", title: "Title" },
            { name: "link", type: "url", title: "Link" },
          ],
        },
      ],
    },
    {
      title: "Address",
      name: "address",
      type: "text",
    },
    {
      title: "Phone",
      name: "phone",
      type: "string",
    },
    {
      title: "Email",
      name: "email",
      type: "string",
    },
    {
      title: "Secondary Email",
      name: "secondaryEmail",
      type: "string",
    },
    {
      title: "Website",
      name: "website",
      type: "string",
    },
    {
      title: "Contact Image",
      name: "contactImage",
      type: "image",
    },
  ],
};
