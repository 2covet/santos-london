import { MdEventAvailable } from "react-icons/md";

export default {
  name: "shows",
  title: "Shows",
  type: "document",
  icon: MdEventAvailable,
  fields: [
    {
      name: "title",
      title: "Title",
      type: "string",
    },
    {
      name: "content",
      title: "Content",
      type: "simplePortableText",
    },
    {
      name: "website",
      title: "Website Link",
      type: "string",
    },
    {
      name: "startDate",
      title: "Start Date",
      validation: (Rule) => Rule.required(),
      type: "date",
    },
    {
      name: "endDate",
      title: "End Date",
      validation: (Rule) => Rule.required(),
      type: "date",
    },
  ],
};
