export default {
  name: "page",
  type: "document",
  title: "Page",
  __experimental_actions: [/*'create',*/ "update", /*"delete",*/ "publish"],
  fieldsets: [
    {
      title: "SEO & metadata",
      name: "metadata",
    },
  ],
  fields: [
    {
      name: "title",
      type: "string",
      title: "Title",
    },
    {
      name: "content",
      type: "array",
      title: "Page sections",
      of: [
        { type: "imageSection" },
        { type: "textSection" },
        { type: "largeImageSection" },
      ],
    },
    {
      name: "seoTitle",
      type: "string",
      title: "SEO Title",
      description: "This title will populate the meta title",
      fieldset: "metadata",
    },
    {
      name: "description",
      type: "text",
      title: "Description",
      description: "This description populates meta-tags on the webpage",
      fieldset: "metadata",
    },
  ],

  preview: {
    select: {
      title: "title",
    },
  },
};
