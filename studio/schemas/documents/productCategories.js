export default {
  name: "productCategories",
  title: "Product Categories",
  type: "document",
  __experimental_actions: [/*"create"*/ "update", /*"delete",*/ "publish"],
  fields: [
    {
      name: "title",
      title: "Title",
      type: "string",
    },
    {
      name: "slug",
      title: "Slug",
      type: "slug",
      options: {
        source: "title",
        maxLength: 96,
      },
    },
    {
      name: "chineseTitle",
      title: "Chinese Title",
      type: "string",
    },
    {
      name: "image",
      title: "Image for category",
      type: "image",
    },
    {
      name: "description",
      title: "Description",
      type: "text",
    },
  ],
};
