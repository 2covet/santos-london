// First, we must import the schema creator
import createSchema from "part:@sanity/base/schema-creator";
// Then import schema types from any plugins that might expose them
import schemaTypes from "all:part:@sanity/base/schema-type";

// We import object and document schemas
import productCategories from "./documents/productCategories";
import linkCategories from "./documents/linkCategories";
import product from "./documents/product";
import page from "./documents/page";
import route from "./documents/route";
import shows from "./documents/shows";
import siteConfig from "./documents/siteConfig";
import book from "./documents/book";
import bookCategory from "./documents/bookCategory";

// Object types
import figure from "./objects/figure";
import internalLink from "./objects/internalLink";
import link from "./objects/link";
import portableText from "./objects/portableText";
import simplePortableText from "./objects/simplePortableText";

// Landing page sections
import imageSection from "./objects/imageSection";
import textSection from "./objects/textSection";
import largeImageSection from "./objects/largeImageSection";

// Then we give our schema to the builder and provide the result to Sanity
export default createSchema({
  // We name our schema
  name: "default",
  // Then proceed to concatenate our document type
  // to the ones provided by any plugins that are installed
  types: schemaTypes.concat([
    // The following are document types which will appear
    // in the studio.
    product,
    book,
    productCategories,
    linkCategories,
    page,
    route,
    shows,
    siteConfig,
    bookCategory,
    // When added to this list, object types can be used as
    figure,
    internalLink,
    link,
    imageSection,
    largeImageSection,
    textSection,
    portableText,
    simplePortableText,
  ]),
});
