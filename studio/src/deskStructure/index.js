import S from "@sanity/desk-tool/structure-builder";
import {
  MdWeb,
  MdSettings,
  MdShoppingBasket,
  MdEventAvailable,
  MdBook,
} from "react-icons/md";

import ProductPagePreview from "../components/previews/product/ProductPagePreview";
// Hide document types that we already have a structure definition for
const hiddenDocTypes = (listItem) =>
  ![
    "productCategories",
    "linkCategories",
    "page",
    "product",
    "book",
    "shows",
    "route",
    "siteConfig",
    "bookCategory",
  ].includes(listItem.getId());

export default () =>
  S.list()
    .title("Santos London")
    .items([
      S.documentTypeListItem("product")
        .title("Products")
        .icon(MdShoppingBasket)
        .child(S.documentList().title("Products").filter('_type == "product"')),
      S.documentTypeListItem("book").title("Books").icon(MdBook),
      S.documentTypeListItem("shows").title("Shows").icon(MdEventAvailable),
      S.listItem()
        .title("Website")
        .icon(MdWeb)
        .child(
          S.list()
            .title("Website")
            .items([
              S.listItem()
                .title("Site configuration")
                .icon(MdSettings)
                .child(
                  S.document()
                    .title("Site configuration")
                    .schemaType("siteConfig")
                    .documentId("siteConfig")
                ),
              S.documentTypeListItem("route").title("Routes"),
              S.documentTypeListItem("page").title("Pages"),
              S.documentTypeListItem("productCategories").title(
                "Product Categories"
              ),
              S.documentTypeListItem("linkCategories").title("Link Categories"),
              S.documentTypeListItem("bookCategory").title("Book Category"),
            ])
        ),
      ...S.documentTypeListItems().filter(hiddenDocTypes),
    ]);

export const getDefaultDocumentNode = (props) => {
  /**
   * Here you can define fallback views for document types without
   * a structure definition for the document node. If you want different
   * fallbacks for different types, or document values (e.g. if there is a slug present)
   * you can set up that logic in here too.
   * https://www.sanity.io/docs/structure-builder-reference#getdefaultdocumentnode-97e44ce262c9
   */
  const { schemaType } = props;
  if (schemaType === "product") {
    return S.document().views([
      S.view.form(),
      S.view.component(ProductPagePreview).title("Product Page"),
    ]);
  }
  return S.document().views([S.view.form()]);
};
