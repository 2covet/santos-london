import S from '@sanity/desk-tool/structure-builder';

export default S.listItem()
  .title('Product Categories')
  .schemaType('productCategories')
  .child(S.documentTypeList('productCategories').title('Product Categories'));
