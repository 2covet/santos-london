import Error from "next/error";
import Head from "next/head";
import { groq } from "next-sanity";
import { useRouter } from "next/router";
import LandingPage from "../components/LandingPage";
import ProductCategories from "../components/ProductCategories";
import { getClient } from "../utils/sanity";

const query = groq`*[_id == '9c0851ae-8b3b-44df-aec1-ec5ec4d5936c']`;

const siteConfigQuery = groq`*[_type == "siteConfig"]`;

const categoriesQuery = groq`*[_type == "productCategories"] | order(_createdAt asc)`;

const bookCategoryQuery = groq`*[_id == 'e472283c-3fa1-464f-b1f8-707d007bfe87']`;

function IndexPage({
  pageData,
  siteConfigData,
  categoryData,
  bookCategoryData,
}) {
  const router = useRouter();
  if (!router.isFallback && !pageData) {
    return <Error statusCode={404} />;
  }

  const allCats = categoryData.concat(bookCategoryData);

  return (
    <>
      <Head>
        <title>{pageData[0].seoTitle}</title>
        <meta name="description" content={pageData[0].description} />
      </Head>
      <div className="container mx-auto px-6">
        <h2 className="text-center font-bold text-3xl mb-5 font-spectral">
          Categories
        </h2>
        <ProductCategories categories={allCats} />
        <div className="md:mt-24 mt-12">
          <LandingPage page={pageData[0]} />
        </div>
      </div>
    </>
  );
}

export async function getServerSideProps() {
  const siteConfigData = await getClient().fetch(siteConfigQuery);

  const pageData = await getClient().fetch(query);

  const categoryData = await getClient().fetch(categoriesQuery);

  const bookCategoryData = await getClient().fetch(bookCategoryQuery);

  return {
    props: {
      pageData,
      siteConfigData,
      categoryData,
      bookCategoryData,
    },
  };
}

export default IndexPage;
