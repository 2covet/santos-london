import Head from "next/head";
import Error from "next/error";
import { groq } from "next-sanity";
import { useRouter } from "next/router";
import LandingPage from "../components/LandingPage";
import { getClient, usePreviewSubscription } from "../utils/sanity";
import ShowsComponent from "../components/Shows";
import ContactComponent from "../components/Contact";

const query = groq`*[_type == "route" && slug.current == $slug][0]{
  page->
}`;

const showsQuery = groq`*[_type == "shows"] | order(_createdAt desc)`;

const siteConfigQuery = groq`*[_type == "siteConfig"]`;

function ProductPageContainer({
  pageData,
  preview,
  slug,
  showsData,
  siteConfigData,
}) {
  const router = useRouter();
  if (!router.isFallback && !pageData) {
    return <Error statusCode={404} />;
  }

  const { data: { page = {} } = {} } = usePreviewSubscription(query, {
    params: { slug },
    initialData: pageData,
    enabled: preview || router.query.preview !== null,
  });

  return (
    <>
      <Head>
        <title>{page.title} | Santos London</title>
      </Head>
      <h1 className="font-spectral font-bold text-4xl text-center">
        {page.title}
      </h1>
      {slug == "shows" ? <ShowsComponent shows={showsData} /> : null}
      {slug == "contact" ? (
        <ContactComponent contactInfo={siteConfigData} />
      ) : null}
      <LandingPage page={page} />
    </>
  );
}

export async function getStaticProps({ params = {}, preview = false }) {
  const { slug } = params;
  const { page: pageData } = await getClient(preview).fetch(query, {
    slug,
  });

  const siteConfigData = await getClient().fetch(siteConfigQuery);

  const showsData = await getClient(preview).fetch(showsQuery);

  return {
    props: {
      preview,
      pageData,
      slug,
      showsData,
      siteConfigData,
    },
    revalidate: 10,
  };
}

export async function getStaticPaths() {
  const routes = await getClient()
    .fetch(`*[_type == "route" && defined(slug.current)]{
    "params": {"slug": slug.current}
  }`);

  return {
    paths: routes || null,
    fallback: true,
  };
}

export default ProductPageContainer;
