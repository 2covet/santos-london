import Error from "next/error";
import Head from "next/head";
import { groq } from "next-sanity";
import { useRouter } from "next/router";
import { getClient } from "../utils/sanity";
import ProductCategories from "../components/ProductCategories";

const siteConfigQuery = `//groq
  *[_type == "siteConfig"]
`;

const categoriesQuery = groq`*[_type == 'productCategories'] | order(_createdAt asc)`;

const bookCategoryQuery = groq`*[_id == 'e472283c-3fa1-464f-b1f8-707d007bfe87']`;

function CategoriesPageContainer({
  siteConfigData,
  categoryData,
  bookCategoryData,
}) {
  const router = useRouter();
  if (!router.isFallback && !categoryData) {
    return <Error statusCode={404} />;
  }

  const allCats = categoryData.concat(bookCategoryData);

  return (
    <>
      <Head>
        <title>Product Categories | Santos London</title>
      </Head>
      <h1 className="font-spectral font-bold text-4xl text-center">
        Chinese Porcelain Catalogue 图录
      </h1>
      <div className="container mx-auto px-6 mt-6">
        <ProductCategories categories={allCats} />
      </div>
    </>
  );
}

export async function getServerSideProps() {
  const siteConfigData = await getClient().fetch(siteConfigQuery);

  const categoryData = await getClient().fetch(categoriesQuery);

  const bookCategoryData = await getClient().fetch(bookCategoryQuery);

  return {
    props: { siteConfigData, categoryData, bookCategoryData },
  };
}

export default CategoriesPageContainer;
