import Head from "next/head";
import Error from "next/error";
import { useRouter } from "next/router";
import { getClient, usePreviewSubscription } from "../../utils/sanity";
import ProductsPage from "../../components/ProductsPage";

const query = `//groq
  *[_type == "product"] | order(_createdAt desc)
`;

const siteConfigQuery = `//groq
  *[_type == "siteConfig"]
`;

function ProductsPageContainer({ productsData, preview, siteConfigData }) {
  const router = useRouter();
  if (!router.isFallback && !productsData) {
    return <Error statusCode={404} />;
  }
  const { data: products } = usePreviewSubscription(query, {
    initialData: productsData,
    enabled: preview || router.query.preview !== null,
  });

  return (
    <>
      <Head>
        <title>All Products | Santos London</title>
      </Head>
      <h1 className="font-spectral font-bold text-4xl text-center">
        New Acquisitions
      </h1>
      <ProductsPage products={products} />
    </>
  );
}

export async function getServerSideProps({ params = {}, preview = false }) {
  const productsData = await getClient(preview).fetch(query);

  const siteConfigData = await getClient().fetch(siteConfigQuery);

  return {
    props: { preview, productsData, siteConfigData },
  };
}

export default ProductsPageContainer;
