import Head from "next/head";
import Error from "next/error";
import { groq } from "next-sanity";
import { useRouter } from "next/router";
import ProductPage from "../../components/ProductPage";
import { getClient, usePreviewSubscription } from "../../utils/sanity";

const query = groq`*[_type == "product" && slug.current == $slug][0]`;

const siteConfigQuery = `//groq
  *[_type == "siteConfig"]
`;

function ProductPageContainer({ productData, preview, siteConfigData }) {
  const router = useRouter();
  if (!router.isFallback && !productData?.slug) {
    return <Error statusCode={404} />;
  }

  const { data: product = {} } = usePreviewSubscription(query, {
    params: { slug: productData?.slug?.current },
    initialData: productData,
    enabled: preview || router.query.preview !== null,
  });

  const {
    _id,
    title,
    price,
    mainImage,
    secondaryImages,
    content,
    sold,
    slug,
    stockNumber,
  } = product;
  return (
    <>
      <Head>
        <title>{title} | Santos London</title>
      </Head>
      <ProductPage
        id={_id}
        title={title}
        price={price}
        mainImage={mainImage}
        secondaryImages={secondaryImages}
        content={content}
        slug={slug?.current}
        sold={sold}
        stockNumber={stockNumber}
        email={siteConfigData[0].secondaryEmail}
      />
    </>
  );
}

export async function getStaticProps({ params, preview = false }) {
  const productData = await getClient(preview).fetch(query, {
    slug: params.slug,
  });

  const siteConfigData = await getClient().fetch(siteConfigQuery);

  return {
    props: { preview, productData, siteConfigData },
    revalidate: 10,
  };
}

export async function getStaticPaths() {
  const paths = await getClient().fetch(
    `*[_type == "product" && defined(slug.current)][].slug.current`
  );

  return {
    paths: paths.map((slug) => ({ params: { slug } })),
    fallback: true,
  };
}

export default ProductPageContainer;
