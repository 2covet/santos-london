import Head from "next/head";
import algoliasearch from "algoliasearch";
import { withInstantSearch } from "next-instantsearch";
import { Configure, Pagination, SearchBox } from "react-instantsearch-dom";
import CustomHits from "../components/algolia/CustomHits";
import { groq } from "next-sanity";
import { getClient } from "../utils/sanity";

const searchClient = algoliasearch(
  "C49MPH9KXR",
  "42e7ac3ed768561cbb2e4b8990353c4e"
);

const siteConfigQuery = groq`*[_type == "siteConfig"]`;

function SearchProducts({ siteConfigData, searchQuery }) {
  return (
    <>
      <Head>
        <title>{searchQuery} results | Santos London</title>
      </Head>
      <div className="container mx-auto px-6">
        <h2 className="text-center font-bold text-3xl mb-5 font-spectral">
          Search results for: {searchQuery}
        </h2>
        <Configure hitsPerPage={12} />
        <SearchBox />
        <CustomHits />
        <Pagination className="mt-6 w-fit-content mx-auto" />
      </div>
    </>
  );
}

SearchProducts.getInitialProps = async (ctx) => {
  const siteConfigData = await getClient().fetch(siteConfigQuery);
  const searchQuery = ctx.query.query;
  return {
    indexName: "dev_PRODUCTS",
    siteConfigData,
    searchQuery,
  };
};

export default withInstantSearch({
  searchClient,
})(SearchProducts);
