import Head from "next/head";
import Error from "next/error";
import { groq } from "next-sanity";
import { useRouter } from "next/router";
import { getClient, usePreviewSubscription } from "../../utils/sanity";
import ProductsPage from "../../components/ProductsPage";

const productQuery = groq`*[_type == 'product' && (sold)] | order(_createdAt desc)`;

const siteConfigQuery = groq`
  *[_type == "siteConfig"]
`;

function ArchivePage({ productsData, preview, siteConfigData }) {
  const router = useRouter();
  if (!router.isFallback && !productsData) {
    return <Error statusCode={404} />;
  }
  const { data: products } = usePreviewSubscription(productQuery, {
    initialData: productsData,
    enabled: preview || router.query.preview !== null,
  });

  return (
    <>
      <Head>
        <title>Sold Stock | Santos London</title>
      </Head>
      <div className="my-8">
        <div className="container mx-auto px-6">
          <h1 className="font-spectral font-bold text-4xl text-center">
            Sold stock 已售藏品
          </h1>
          <ProductsPage products={products} />
        </div>
      </div>
    </>
  );
}

export async function getServerSideProps({ params, preview = false }) {
  const siteConfigData = await getClient().fetch(siteConfigQuery);

  const productsData = await getClient(preview).fetch(productQuery);

  return {
    props: { preview, siteConfigData, productsData },
  };
}

export default ArchivePage;
