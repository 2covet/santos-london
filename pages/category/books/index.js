import Head from "next/head";
import Error from "next/error";
import { groq } from "next-sanity";
import { useRouter } from "next/router";
import { getClient, usePreviewSubscription } from "../../../utils/sanity";
import BooksPage from "../../../components/BooksPage";

const query = groq`*[_type == 'book' && defined(slug.current)] | order(_createdAt asc)`;

const siteConfigQuery = groq`*[_type == 'siteConfig']`;

function BooksPageContainer({ booksData, preview, siteConfigData }) {
  const router = useRouter();
  if (!router.isFallback && !booksData) {
    return <Error statusCode={404} />;
  }
  const { data: books } = usePreviewSubscription(query, {
    initialData: booksData,
    enabled: preview || router.query.preview !== null,
  });

  return (
    <>
      <Head>
        <title>Books &amp; Catalogues | Santos London</title>
      </Head>
      <h1 className="font-spectral font-bold text-4xl text-center">
        Books &amp; Catalogues
      </h1>
      <BooksPage books={books} />
    </>
  );
}

export async function getServerSideProps({ params = {}, preview = false }) {
  const booksData = await getClient(preview).fetch(query);

  const siteConfigData = await getClient().fetch(siteConfigQuery);

  return {
    props: { preview, booksData, siteConfigData },
  };
}

export default BooksPageContainer;
