import Head from "next/head";
import Error from "next/error";
import { groq } from "next-sanity";
import { useRouter } from "next/router";
import BookPage from "../../../components/BookPage";
import { getClient, usePreviewSubscription } from "../../../utils/sanity";

const query = groq`*[_type == 'book' && slug.current == $slug][0]`;

const siteConfigQuery = groq`*[_type == 'siteConfig']`;

function BookPageContainer({ bookData, preview, siteConfigData }) {
  const router = useRouter();
  if (!router.isFallback && !bookData?.slug) {
    return <Error statusCode={404} />;
  }

  const { data: book = {} } = usePreviewSubscription(query, {
    params: { slug: bookData?.slug?.current },
    initialData: bookData,
    enabled: preview || router.query.preview !== null,
  });

  const { _id, slug, title, mainImage, secondaryImages, content, pdfDownload } =
    book;
  return (
    <>
      <Head>
        <title>{title} | Santos London</title>
      </Head>
      <BookPage
        id={_id}
        slug={slug?.current}
        title={title}
        mainImage={mainImage}
        secondaryImages={secondaryImages}
        content={content}
        pdfDownload={pdfDownload}
      />
    </>
  );
}

export async function getStaticProps({ params, preview = false }) {
  const bookData = await getClient(preview).fetch(query, {
    slug: params.slug,
  });

  const siteConfigData = await getClient().fetch(siteConfigQuery);

  return {
    props: { preview, bookData, siteConfigData },
    revalidate: 10,
  };
}

export async function getStaticPaths() {
  const paths = await getClient().fetch(
    `*[_type == 'book' && defined(slug.current)][].slug.current`
  );

  return {
    paths: paths.map((slug) => ({ params: { slug } })),
    fallback: true,
  };
}

export default BookPageContainer;
