import Head from "next/head";
import Error from "next/error";
import { groq } from "next-sanity";
import { useRouter } from "next/router";
import { getClient, usePreviewSubscription } from "../../utils/sanity";
import ProductsPage from "../../components/ProductsPage";

const query = groq`*[_type == "productCategories" && slug.current == $slug][0]`;

const productQuery = groq`*[_type == 'product' && defined(slug.current) && (!(sold))] | order(_createdAt desc)`;

const siteConfigQuery = `//groq
  *[_type == "siteConfig"]
`;

function CategoryPageContainer({
  categoryData,
  productsData,
  preview,
  siteConfigData,
}) {
  const router = useRouter();
  if (!router.isFallback && !categoryData?.slug) {
    return <Error statusCode={404} />;
  }

  const { data: category = {} } = usePreviewSubscription(query, {
    params: { slug: categoryData?.slug?.current },
    initialData: categoryData,
    enabled: preview || router.query.preview !== null,
  });

  const ProductFinder = (cb) => {
    return productsData.filter((o) => {
      if (o.categories[0] && o.categories[0]._ref) {
        return o.categories[0]._ref.includes(cb);
      }
      return false;
    });
  };

  return (
    <>
      <Head>
        <title>{category.title} | Santos London</title>
      </Head>
      <div className="my-8">
        <div className="container mx-auto px-6">
          <h1 className="font-spectral font-bold text-4xl text-center leading-normal">
            {category.title}
            <br />
            {category.chineseTitle}
          </h1>
          <ProductsPage products={ProductFinder(category._id)} />
        </div>
      </div>
    </>
  );
}

export async function getStaticProps({ params, preview = false }) {
  const categoryData = await getClient(preview).fetch(query, {
    slug: params.slug,
  });

  const productsData = await getClient(preview).fetch(productQuery);

  const siteConfigData = await getClient().fetch(siteConfigQuery);

  return {
    props: { preview, categoryData, siteConfigData, productsData },
    revalidate: 10,
  };
}

export async function getStaticPaths() {
  const paths = await getClient().fetch(
    `*[_type == "productCategories" && defined(slug.current)][].slug.current`
  );

  return {
    paths: paths.map((slug) => ({ params: { slug } })),
    fallback: true,
  };
}

export default CategoryPageContainer;
