import Head from "next/head";
import Error from "next/error";
import { useRouter } from "next/router";
import "../styles/index.css";
import Layout from "../components/Layout";

import 'react-inner-image-zoom/lib/InnerImageZoom/styles.min.css';
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";

import 'instantsearch.css/themes/reset.css';
import 'instantsearch.css/themes/satellite.css';


function MyApp({ Component, pageProps }) {
  const router = useRouter();

  if (!pageProps.siteConfigData) {
    return <Error statusCode={404} />;
  }

  return (
    <Layout siteData={pageProps.siteConfigData}>
      <Head>
        <link rel="preconnect" href="https://fonts.gstatic.com" />
        <link
          href="https://fonts.googleapis.com/css2?family=Karla:ital,wght@0,400;0,700;1,400&family=Spectral:wght@700&display=swap"
          rel="stylesheet"
        />
        <link rel="shortcut icon" href="/favicon.ico" />
      </Head>
      <Component {...pageProps} />
    </Layout>
  );
}

export default MyApp;
