import algoliasearch from "algoliasearch";
import sanityClient from "@sanity/client";
import indexer from "sanity-algolia";

const algolia = algoliasearch("C49MPH9KXR", "8b9f1b8da49e4a4bee28e0bfc717eb53");
const sanity = sanityClient({
  projectId: "dtp0zcpt",
  dataset: "production",
  apiVersion: "2021-03-25",
  useCdn: false,
});

/**
 *  This function receives webhook POSTs from Sanity and updates, creates or
 *  deletes records in the corresponding Algolia indices.
 */
const handler = (req, res) => {
  // Tip: Its good practice to include a shared secret in your webhook URLs and
  // validate it before proceeding with webhook handling. Omitted in this short
  // example.
  if (req.headers["content-type"] !== "application/json") {
    res.status(400);
    res.json({ message: "Bad request" });
    return;
  }

  // Configure this to match an existing Algolia index name
  const algoliaIndex = algolia.initIndex("dev_PRODUCTS");

  const sanityAlgolia = indexer(
    // The first parameter maps a Sanity document type to its respective Algolia
    // search index. In this example both `post` and `article` Sanity types live
    // in the same Algolia index. Optionally you can also customize how the
    // document is fetched from Sanity by specifying a GROQ projection.
    //
    // In this example we fetch the plain text from Portable Text rich text
    // content via the pt::text function.
    //
    // _id and other system fields are handled automatically.
    {
      product: {
        index: algoliaIndex,
        projection: `{
          title,
          slug,
          mainImage,
          sold,
          price
        }`,
      },
    },

    // The second parameter is a function that maps from a fetched Sanity document
    // to an Algolia Record. Here you can do further mutations to the data before
    // it is sent to Algolia.
    (document) => {
      switch (document._type) {
        case "product":
          return {
            title: document.title,
            slug: document.slug.current,
            image: document.mainImage,
            sold: document.sold,
            price: document.price,
          };
        default:
          return document;
      }
    }
  );

  // Finally connect the Sanity webhook payload to Algolia indices via the
  // configured serializers and optional visibility function. `webhookSync` will
  // inspect the webhook payload, make queries back to Sanity with the `sanity`
  // client and make sure the algolia indices are synced to match.
  return sanityAlgolia
    .webhookSync(sanity, req.body)
    .then(() => res.status(200).send("ok"));
};

export default handler;
