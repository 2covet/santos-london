const STUDIO_REWRITE = {
  source: "/studio/:path*",
  destination:
    process.env.NODE_ENV === "development"
      ? "http://localhost:3333/studio/:path*"
      : "/studio/index.html",
};

module.exports = {
  rewrites: () => [STUDIO_REWRITE],
  webpack5: true,
  async redirects() {
    return [
      {
        source: "/chinese-porcelain-stock.asp",
        destination: "/categories",
        permanent: true,
      },
      {
        source: "/contact.asp",
        destination: "/contact",
        permanent: true,
      },
      {
        source: "/antique-dealer-links.asp",
        destination: "/",
        permanent: true,
      },
      {
        source: "/antique-shows.asp",
        destination: "/shows",
        permanent: true,
      },
      {
        source: "/new-chinese-porcelain-acquisitions.asp",
        destination: "/categories",
        permanent: true,
      },
      {
        source: "/santos-london-porcelain.asp",
        destination: "/",
        permanent: true,
      },
      {
        source: "/chinese-porcelain-details.asp",
        destination: "/",
        permanent: true,
      },
    ];
  },
};
