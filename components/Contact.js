import React from "react";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faPhone,
  faEnvelope,
  faMapMarkerAlt,
  faSearch,
  faGlobe,
} from "@fortawesome/free-solid-svg-icons";

import { urlFor } from "../utils/sanity";

const ContactComponent = ({ contactInfo }) => {
  return (
    <div className="container mx-auto mt-6">
      <div className="flex flex-col md:flex-row">
        <div className="md:w-1/2 w-full bg-lightGrey p-8">
          <h3 className="mb-3 font-spectral font-bold text-2xl">Contact Us</h3>
          <ul>
            <li className="flex items-center mb-3">
              <FontAwesomeIcon
                icon={faMapMarkerAlt}
                listItem
                width="16"
                height="16"
                className="mr-2"
              />{" "}
              {contactInfo[0].address}
            </li>
            <li className="flex items-center mb-3">
              <FontAwesomeIcon
                icon={faPhone}
                listItem
                width="16"
                height="16"
                className="mr-2"
              />{" "}
              <a href={`tel:${contactInfo[0].phone}`}>{contactInfo[0].phone}</a>
            </li>
            <li className="flex items-center mb-3">
              <FontAwesomeIcon
                icon={faEnvelope}
                listItem
                width="16"
                height="16"
                className="mr-2"
              />{" "}
              <a href={`mailto:${contactInfo[0].secondaryEmail}`}>
                {contactInfo[0].secondaryEmail}
              </a>
            </li>
          </ul>
        </div>
        <div
          style={{
            backgroundImage: `url(${urlFor(contactInfo[0].contactImage)})`,
          }}
          className="md:w-1/2 w-full h-96 md:h-auto bg-cover bg-center"
        />
      </div>
    </div>
  );
};

export default ContactComponent;
