import Link from "next/link";
import { urlFor } from "../utils/sanity";

function BookCard({ title, mainImage, slug }) {
  return (
    <div className="w-full bg-lightGrey max-w-sm mx-auto shadow-md hover:shadow-xl overflow-hidden transition duration-300 ease-in-out">
      <Link href={`/category/books/${slug.current}`}>
        <a>
          <div
            className="flex items-end justify-end h-96 w-full bg-cover"
            style={{
              backgroundImage: `url('${urlFor(mainImage)
                .auto("format")
                .fit("crop")
                .width("750")
                .quality(80)}')`,
            }}
          />
          <div className="px-5 py-3">
            <h3 className="font-spectral text-black">{title}</h3>
          </div>
        </a>
      </Link>
    </div>
  );
}

export default BookCard;
