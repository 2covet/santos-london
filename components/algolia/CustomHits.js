import Link from "next/link";
import { connectHits } from "react-instantsearch-dom";
import { urlFor } from "../../utils/sanity";

const Hits = ({ hits }) => (
  <div className="grid gap-6 grid-cols-1 sm:grid-cols-2 lg:grid-cols-3 xl:grid-cols-4 mt-6">
    {hits.map((hit) => (
      <div
        className="w-full bg-lightGrey max-w-sm mx-auto shadow-md hover:shadow-xl overflow-hidden transition duration-300 ease-in-out"
        key={hit.objectID}
      >
        <Link href={`/products/${hit.slug}`}>
          <a>
            <div
              className="flex items-end justify-end h-56 w-full bg-cover"
              style={{
                backgroundImage: `url('${urlFor(hit.image)
                  .auto("format")
                  .fit("crop")
                  .width(750)
                  .quality(80)}')`,
              }}
            ></div>
            <div className="px-5 py-3">
              <h3 className="font-spectral text-black">{hit.title}</h3>
            </div>
          </a>
        </Link>
      </div>
    ))}
  </div>
);

const CustomHits = connectHits(Hits);

export default CustomHits;
