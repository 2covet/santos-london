import Slider from "react-slick";
import { urlFor, PortableText, getUrlFromId } from "../utils/sanity";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faChevronLeft,
  faChevronRight,
  faDownload,
} from "@fortawesome/free-solid-svg-icons";

function NextArrow(props) {
  const { onClick } = props;
  return (
    <div
      className="product-slider-arrow-next transition duration-300 ease-in-out hover:opacity-40"
      onClick={onClick}
    >
      <FontAwesomeIcon icon={faChevronRight} width="18" />
    </div>
  );
}

function PrevArrow(props) {
  const { onClick } = props;
  return (
    <div
      className="product-slider-arrow-prev transition duration-300 ease-in-out hover:opacity-40"
      onClick={onClick}
    >
      <FontAwesomeIcon icon={faChevronLeft} width="18" />
    </div>
  );
}

function BookPage(props) {
  const { title, mainImage, secondaryImages, content, pdfDownload } = props;

  const settings = {
    dots: true,
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 2000,
    adaptiveHeight: true,
    nextArrow: <NextArrow />,
    prevArrow: <PrevArrow />,
  };

  let imagesData = [];

  imagesData.push(mainImage);

  const finalImages = secondaryImages
    ? imagesData.concat(secondaryImages)
    : imagesData;

  return (
    <div className="container mx-auto px-6">
      <div className="md:flex md:items-start">
        <div className="w-full md:w-1/2">
          <Slider {...settings}>
            {finalImages.map((image, index) => (
              <img key={index} src={urlFor(image).auto("format").url()} />
            ))}
          </Slider>
        </div>
        <div className="w-full mx-auto mt-5 md:ml-10 md:mt-0 md:w-1/2">
          <h1 className="text-2xl font-spectral mb-5">{title}</h1>
          {content && (
            <PortableText blocks={content} className="mb-5 portableText" />
          )}
          <div className="flex flex-col items-start">
            {pdfDownload && (
              <a
                href={getUrlFromId(pdfDownload.asset._ref)}
                target="_blank"
                className="text-white bg-primary p-4 border-2 border-black cursor-pointer mt-4"
              >
                <FontAwesomeIcon
                  icon={faDownload}
                  width="18"
                  className="mr-2 inline-block"
                />
                Download PDF
              </a>
            )}
          </div>
        </div>
      </div>
    </div>
  );
}

export default BookPage;
