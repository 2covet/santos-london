import BookCard from "./BookCard";

function BooksPage({ books }) {
  return (
    <div className="container mx-auto px-6">
      <div className="grid gap-6 grid-cols-1 sm:grid-cols-2 lg:grid-cols-3 xl:grid-cols-4 mt-6">
        {books.map((book) => (
          <BookCard key={book._id} {...book} />
        ))}
      </div>
    </div>
  );
}

export default BooksPage;
