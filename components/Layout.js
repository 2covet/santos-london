import { useState } from "react";
import Router from "next/router";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faPhone,
  faEnvelope,
  faMapMarkerAlt,
  faSearch,
  faGlobe,
} from "@fortawesome/free-solid-svg-icons";
import Link from "next/link";
import ActiveLink from "../utils/activeLink";

import { urlFor } from "../utils/sanity";

function Layout(props) {
  const { children, siteData } = props;
  const [menuOpen, setMenuOpen] = useState(false);
  const handleMenu = () => setMenuOpen(!menuOpen);
  const handleSearch = (e) => {
    e.preventDefault();
    const form = new FormData(e.target);
    const value = form.get("s");

    Router.push({
      pathname: "/search-products",
      query: { query: value },
    });
  };
  return (
    <>
      <div className="bg-black">
        <div className="container mx-auto md:px-6 px-0">
          <div className="block md:flex items-center justify-between">
            <div className="w-full md:text-left text-center">
              <span className="border-r-2 border-gray-200 text-gray-200 px-4 py-3 inline-flex items-center">
                <FontAwesomeIcon icon={faPhone} width="16" className="mr-2" />{" "}
                <a href={`tel:${siteData[0].phone}`}>{siteData[0].phone}</a>
              </span>
              <span className="text-gray-200 px-4 py-3 inline-flex items-center">
                <FontAwesomeIcon
                  icon={faEnvelope}
                  width="16"
                  className="mr-2"
                />{" "}
                <a href={`mailto:${siteData[0].secondaryEmail}`}>
                  {siteData[0].secondaryEmail}
                </a>
              </span>
            </div>
            <div>
              <form
                onSubmit={handleSearch}
                role="search"
                className="flex md:pt-0 pt-6 md:pb-0 pb-6 justify-center"
              >
                <input
                  type="search"
                  name="s"
                  placeholder="Search here..."
                  autoComplete="off"
                  className="border-0 bg-search text-white appearance-none"
                />
                <button
                  type="submit"
                  title="Search"
                  aria-label="Search"
                  disabled=""
                  className="bg-search p-4 border-l-2 border-black transition duration-300 ease-in-out hover:bg-primary"
                >
                  <FontAwesomeIcon
                    icon={faSearch}
                    width="16"
                    className="text-search"
                  />
                </button>
              </form>
            </div>
          </div>
        </div>
      </div>
      <div className="bg-santosGrey pt-6">
        <header className="pb-6">
          <div className="container mx-auto px-6 py-3">
            <div className="flex items-center justify-between md:justify-center">
              <a href="/">
                <img
                  className="h-full w-60 md:w-96 object-cover mx-auto"
                  src={urlFor(siteData[0].logo)}
                  alt={siteData[0].logo.alt}
                />
              </a>
              <div className="flex sm:hidden">
                <button
                  onClick={handleMenu}
                  type="button"
                  className="text-gray-600 hover:text-gray-500 focus:outline-none focus:text-gray-500"
                  aria-label="toggle menu"
                >
                  <svg viewBox="0 0 24 24" className="h-6 w-6 fill-current">
                    <path
                      fillRule="evenodd"
                      d="M4 5h16a1 1 0 0 1 0 2H4a1 1 0 1 1 0-2zm0 6h16a1 1 0 0 1 0 2H4a1 1 0 0 1 0-2zm0 6h16a1 1 0 0 1 0 2H4a1 1 0 0 1 0-2z"
                    />
                  </svg>
                </button>
              </div>
            </div>
            <nav
              className={`${
                menuOpen ? "" : "hidden"
              } sm:flex sm:justify-center sm:items-center mt-4 border-b-2 border-t-2 border-black`}
            >
              <div className="flex flex-col sm:flex-row text-center">
                <ActiveLink activeClassName="bg-primary text-white" href="/">
                  <a className="text-black hover:bg-primary hover:text-white uppercase border-black border-r-0 md:border-r-2 p-4 transition duration-300 ease-in-out">
                    Home
                    <br />
                    主页
                  </a>
                </ActiveLink>
                <ActiveLink
                  activeClassName="bg-primary text-white"
                  href="/categories"
                >
                  <a className="text-black hover:bg-primary hover:text-white uppercase border-black border-r-0 md:border-r-2 p-4 transition duration-300 ease-in-out">
                    Chinese Ceramics
                    <br />
                    中国陶瓷
                  </a>
                </ActiveLink>
                <ActiveLink
                  activeClassName="bg-primary text-white"
                  href="/category/books"
                >
                  <a className="text-black hover:bg-primary hover:text-white uppercase border-black border-r-0 md:border-r-2 p-4 transition duration-300 ease-in-out">
                    Books &amp; Catalogues
                    <br />
                    书藉与图录
                  </a>
                </ActiveLink>
                <ActiveLink
                  activeClassName="bg-primary text-white"
                  href="/shows"
                >
                  <a className="text-black hover:bg-primary hover:text-white uppercase border-black border-r-0 md:border-r-2 p-4 transition duration-300 ease-in-out">
                    Shows
                    <br />
                    古董展
                  </a>
                </ActiveLink>
                <ActiveLink
                  activeClassName="bg-primary text-white"
                  href="/contact"
                >
                  <a className="text-black hover:bg-primary hover:text-white uppercase p-4 transition duration-300 ease-in-out">
                    Contact Us
                    <br />
                    联系方式
                  </a>
                </ActiveLink>
              </div>
            </nav>
          </div>
        </header>
        <main>{children}</main>
        <footer className="container mx-auto border-t border-black border-b mt-16">
          <div className="py-6 flex justify-between flex-col md:flex-row">
            <div className="flex-1 px-6">
              <h3 className="mb-2 font-spectral text-3xl">Index 目录</h3>
              <ul>
                <li>
                  <ActiveLink activeClassName="font-bold" href="/">
                    <a>- Home 主页</a>
                  </ActiveLink>
                </li>
                <li>
                  <ActiveLink activeClassName="font-bold" href="/categories">
                    <a>- Chinese Ceramics 图录</a>
                  </ActiveLink>
                </li>
                <li>
                  <ActiveLink
                    activeClassName="font-bold"
                    href="/category/archive"
                  >
                    <a>- Sold stock 已售藏品</a>
                  </ActiveLink>
                </li>
                <li>
                  <ActiveLink
                    activeClassName="font-bold"
                    href="/category/books"
                  >
                    <a>- Books &amp; Catalogues 书藉与图录</a>
                  </ActiveLink>
                </li>
                <li>
                  <ActiveLink activeClassName="font-bold" href="/shows">
                    <a>- Shows 古董展</a>
                  </ActiveLink>
                </li>
                <li>
                  <ActiveLink activeClassName="font-bold" href="/contact">
                    <a>- Contact Us 联系方式</a>
                  </ActiveLink>
                </li>
              </ul>
            </div>
            <div className="flex-1 px-6">
              <h3 className="mb-2 font-spectral text-3xl">
                Associates 关连公司
              </h3>
              <div className="flex flex-row align-top justify-evenly">
                {siteData[0].associates.map((image, index) => (
                  <a
                    href={image.link}
                    target="_blank"
                    className="text-center"
                    key={index}
                  >
                    <p className="mb-1">
                      <small>{image.title}</small>
                    </p>
                    <img
                      className="mx-2"
                      src={urlFor(image).auto("format").height(50)}
                    />
                  </a>
                ))}
              </div>
              <div className="mt-3">
                <p>Photography:<br />Allerton Photography<br /> Emma van Lindholm</p>
                <br />
                <p>Translations:<br />Louis Hung Pui Ming • 翻译: 洪焙铭<br/> Qiandan Wu • 翻译: 倩丹</p>
              </div>
            </div>
            <div className="flex-1 px-6">
              <h3 className="mb-2 font-spectral text-3xl">
                Contact Us 联系方式
              </h3>
              <ul>
                <li className="flex items-center mb-3">
                  <FontAwesomeIcon
                    icon={faMapMarkerAlt}
                    listItem
                    width="16"
                    height="16"
                    className="mr-2"
                  />{" "}
                  {siteData[0].address}
                </li>
                <li className="flex items-center mb-3">
                  <FontAwesomeIcon
                    icon={faPhone}
                    listItem
                    width="16"
                    height="16"
                    className="mr-2"
                  />{" "}
                  <a href={`tel:${siteData[0].phone}`}>{siteData[0].phone}</a>
                </li>
                <li className="flex items-center mb-3">
                  <FontAwesomeIcon
                    icon={faEnvelope}
                    listItem
                    width="16"
                    height="16"
                    className="mr-2"
                  />{" "}
                  <a href={`mailto:${siteData[0].secondaryEmail}`}>
                    {siteData[0].secondaryEmail}
                  </a>
                </li>
                <li className="flex items-center mb-3">
                  <FontAwesomeIcon
                    icon={faGlobe}
                    listItem
                    width="16"
                    height="16"
                    className="mr-2"
                  />{" "}
                  <Link href="/">
                    <a>{siteData[0].website}</a>
                  </Link>
                </li>
              </ul>
            </div>
          </div>
        </footer>
        <footer className="container mx-auto py-2">
          <p>
            &copy; SANTOS-London - Design and build by{" "}
            <a href="https://theantiquemarketingcompany.co.uk/" target="_blank" className="underline">
            The Antique Marketing Company
            </a>
          </p>
        </footer>
      </div>
    </>
  );
}

export default Layout;
