import InnerImageZoom from "react-inner-image-zoom";
import Slider from "react-slick";
import { urlFor, PortableText } from "../utils/sanity";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faPencilAlt,
  faSearchPlus,
  faChevronLeft,
  faChevronRight,
} from "@fortawesome/free-solid-svg-icons";
import React from "react";

function NextArrow(props) {
  const { onClick } = props;
  return (
    <div
      className="product-slider-arrow-next transition duration-300 ease-in-out hover:opacity-40"
      onClick={onClick}
    >
      <FontAwesomeIcon icon={faChevronRight} width="18" />
    </div>
  );
}

function PrevArrow(props) {
  const { onClick } = props;
  return (
    <div
      className="product-slider-arrow-prev transition duration-300 ease-in-out hover:opacity-40"
      onClick={onClick}
    >
      <FontAwesomeIcon icon={faChevronLeft} width="18" />
    </div>
  );
}

function ProductPage(props) {
  const {
    title,
    price,
    mainImage,
    secondaryImages,
    content,
    email,
    sold,
    stockNumber,
  } = props;

  const settings = {
    dots: true,
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplay: false,
    autoplaySpeed: 2000,
    adaptiveHeight: false,
    nextArrow: <NextArrow />,
    prevArrow: <PrevArrow />,
  };

  let imagesData = [];

  imagesData.push(mainImage);

  const finalImages = secondaryImages
    ? imagesData.concat(secondaryImages)
    : imagesData;

  return (
    <div className="container mx-auto px-6">
      <div className="md:flex md:items-start">
        <div className="w-full md:w-1/2">
          <Slider {...settings}>
            {finalImages.map((image, index) => (
              <React.Fragment key={index}>
                <InnerImageZoom
                  src={urlFor(image)
                    .auto("format")
                    .width(1051)
                    .quality(60)
                    .url()}
                  zoomSrc={urlFor(image).auto("format").url()}
                />
              </React.Fragment>
            ))}
          </Slider>
          <p className="text-center mt-6">
            <FontAwesomeIcon
              icon={faSearchPlus}
              width="18"
              className="mr-2 inline-block"
            />{" "}
            Click the above image to zoom
          </p>
        </div>
        <div className="w-full mx-auto mt-5 md:ml-10 md:mt-0 md:w-1/2">
          <h1 className="text-2xl font-spectral mb-5">{title}</h1>
          {sold ? (
            <h3 className="text-3xl font-spectral mb-5 text-primary">Sold</h3>
          ) : null}
          {stockNumber ? (
            <h3 className="text-2xl font-spectral mb-5 text-primary">
              Stock Number: {stockNumber}
            </h3>
          ) : null}
          {content && (
            <PortableText blocks={content} className="mb-5 portableText" />
          )}
          <div className="flex flex-col items-start">
            <a
              href={`mailto:${email}?subject=Website%20Enquiry:%20${title}`}
              className="text-white bg-primary p-4 border-2 border-black cursor-pointer mt-4"
            >
              <FontAwesomeIcon
                icon={faPencilAlt}
                width="16"
                className="mr-2 inline-block"
              />{" "}
              Enquire
            </a>
          </div>
        </div>
      </div>
    </div>
  );
}

export default ProductPage;
