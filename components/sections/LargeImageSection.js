import PropTypes from "prop-types";
import { PortableText, urlFor } from "../../utils/sanity";

function ImageSection(props) {
  const { heading, text, largeImage, smallImage } = props;

  if (!largeImage || !smallImage) {
    return null;
  }

  return (
    <div className="mt-6">
      <div className="container mx-auto px-6">
        <div className="flex items-start flex-col justify-between md:flex-row">
          <div className="w-full md:w-4/12 relative">
            <img className="object-cover w-full mb-6" src={urlFor(largeImage).auto("format").width(430).url()} alt="heading" />
            <img className="absolute top-2/4 right-0 transform -translate-y-2/4 translate-x-2/4 border-8 border-white hidden md:block" src={urlFor(smallImage).auto('format').width(200).url()} />
          </div>
          <div className="ml-0 md:ml-32 md:w-7/12 w-full">
            <h1 className="font-spectral font-bold text-2xl mb-4">{heading}</h1>
            {text && <PortableText className="portableText" blocks={text} />}
          </div>
        </div>
      </div>
    </div>
  );
}

ImageSection.propTypes = {
  heading: PropTypes.string,
  text: PropTypes.array,
  largeImage: PropTypes.shape({
    asset: PropTypes.shape({
      _ref: PropTypes.string,
    }),
  }),
  smallImage: PropTypes.shape({
    asset: PropTypes.shape({
      _ref: PropTypes.string,
    })
  })
};

export default ImageSection;
