import PropTypes from "prop-types";
import { PortableText, urlFor } from "../../utils/sanity";

function ImageSection(props) {
  const { heading, text, image } = props;

  if (!image) {
    return null;
  }

  return (
    <div className="mt-6">
      <div className="container mx-auto px-6">
        <div>
          <div className="flex items-start flex-col md:flex-row">
            <div className="fancy-background mr-5 mb-6 md:mb-0 w-full md:w-4/6">
              <img
                className="object-cover w-full"
                src={urlFor(image).auto("format").width(430).url()}
                alt={heading}
              />
            </div>
            <div className="ml-5 w-full">
              <h2 className="font-spectral font-bold text-2xl mb-4">{heading}</h2>
              {text && <PortableText className="portableText" blocks={text} />}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

ImageSection.propTypes = {
  heading: PropTypes.string,
  text: PropTypes.array,
  image: PropTypes.shape({
    asset: PropTypes.shape({
      _ref: PropTypes.string,
    }),
  }),
};

export default ImageSection;
