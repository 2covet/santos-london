import React, { useState } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faGlobe } from "@fortawesome/free-solid-svg-icons";
import { PortableText } from "../utils/sanity";

const showByDefault = 6;

const ShowsComponent = ({ shows }) => {
  const [showAll, setShowAll] = useState(false);
  const visibleOptions = showAll ? shows.length : showByDefault;
  const toggleShowAll = () => {
    setShowAll(!showAll);
  };

  const today = new Date().toISOString().substring(0, 10);

  const pastShows = shows.filter((item) => {
    return item.endDate <= today;
  });

  const futureShows = shows.filter((item) => {
    return item.endDate >= today;
  });

  return (
    <div className="container mx-auto mt-6">
      <div className="mb-8">
        <h2 className="font-spectral font-bold text-3xl mb-2 text-center">
          Upcoming Shows 即将举办的展会
        </h2>
        <div className="grid grid-cols-1 md:grid-cols-2 gap-4">
          {futureShows.slice(0, visibleOptions).map((show, index) => (
            <div key={index} className="bg-lightGrey p-4">
              <h5 className="font-spectral font-bold text-2xl mb-2">
                {show.title}
              </h5>
              <PortableText className="portableText" blocks={show.content} />
              <a
                href={show.website}
                target="_blank"
                className="text-white bg-primary p-4 border-2 border-black cursor-pointer inline-block mt-4"
              >
                <FontAwesomeIcon
                  icon={faGlobe}
                  width="18"
                  className="mr-2 inline-block"
                />
                Visit website
              </a>
            </div>
          ))}
        </div>
        {futureShows.length > showByDefault && (
          <div className="grid justify-items-center mt-5">
            {!showAll ? (
              <a
                className="text-white bg-primary inline-block p-4 border-2 border-black cursor-pointer"
                href="#"
                onClick={toggleShowAll}
              >
                {`Show ${futureShows.length - showByDefault} More`}
              </a>
            ) : (
              <a
                className="text-white bg-primary inline-block p-4 border-2 border-black cursor-pointer"
                onClick={toggleShowAll}
              >
                {`Show Less`}
              </a>
            )}
          </div>
        )}
      </div>
      <h2 className="font-spectral font-bold text-3xl mb-2 text-center">
        Past Shows 过去的展会
      </h2>
      <div className="grid grid-cols-1 md:grid-cols-2 gap-4">
        {pastShows.slice(0, visibleOptions).map((show, index) => (
          <div key={index} className="bg-lightGrey p-4">
            <h5 className="font-spectral font-bold text-2xl mb-2">
              {show.title}
            </h5>
            <PortableText className="portableText" blocks={show.content} />
          </div>
        ))}
      </div>
      {pastShows.length > showByDefault && (
        <div className="grid justify-items-center mt-5">
          {!showAll ? (
            <a
              className="text-white bg-primary inline-block p-4 border-2 border-black cursor-pointer"
              href="#"
              onClick={toggleShowAll}
            >
              {`Show ${pastShows.length - showByDefault} More`}
            </a>
          ) : (
            <a
              className="text-white bg-primary inline-block p-4 border-2 border-black cursor-pointer"
              onClick={toggleShowAll}
            >
              {`Show Less`}
            </a>
          )}
        </div>
      )}
    </div>
  );
};

export default ShowsComponent;
