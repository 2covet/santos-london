import { urlFor } from "../utils/sanity";

function ProductCategories({ categories }) {
  return (
    <>
      <div className="flex flex-col sm:grid sm:grid-cols-1 lg:grid-cols-3 gap-4">
        {categories.map((item, index) => (
          <a href={`/category/${item.slug.current}`} key={index}>
            <div
              className="h-96 categoriesBlock bg-cover bg-center transition duration-300 ease-in-out"
              style={{
                backgroundImage: `url('${urlFor(item.image)
                  .auto("format")
                  .fit("crop")
                  .width(750)
                  .quality(80)}`,
              }}
            ></div>
            <h3 className="font-spectral text-center mt-4">
              {item.title}
              <br />
              {item.chineseTitle}
            </h3>
          </a>
        ))}
      </div>
    </>
  );
}

export default ProductCategories;
