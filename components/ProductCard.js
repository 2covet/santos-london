import Link from "next/link";
import { urlFor } from "../utils/sanity";

function ProductCard({ title, mainImage, slug, sold }) {
  return (
    <div className="w-full bg-lightGrey max-w-sm mx-auto shadow-md hover:shadow-xl overflow-hidden transition duration-300 ease-in-out relative">
      <Link href={`/products/${slug.current}`}>
        <a>
          <div
            className="flex items-end justify-end h-96 w-full bg-cover bg-center"
            style={{
              backgroundImage: `url('${urlFor(mainImage)
                .auto("format")
                .fit("crop")
                .width(750)
                .quality(80)}`,
            }}
          ></div>
          <div className="px-5 py-3">
            <h3 className="font-spectral text-black">{title}</h3>
          </div>
        </a>
      </Link>
      {sold ? (
        <span className="inline-flex items-center justify-center px-2 py-1 text-xs font-bold leading-none text-white bg-primary rounded absolute top-4 right-4">
          SOLD
        </span>
      ) : null}
    </div>
  );
}

export default ProductCard;
